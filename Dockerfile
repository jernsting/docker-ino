FROM python:3

LABEL maintainer = "j.ernsting@uni-muenster.de"
LABEL version = "1.0"

RUN apt-get update -q \
  && DEBIAN_FRONTEND=noninteractive apt-get -yqq install arduino-core\
  && pip install ino

WORKDIR /root
